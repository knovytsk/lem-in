# lem-in #
The goal of this project is to find the quickest way to get n ants across the farm.

### skills ###
- graphs represantation
- BFS algorithm

### usage ###
./lem-in [map]

### ANT FARM EDITOR ###
- cd antfarm_creator

- make

- ./antfarm.creator
	
	- draw room: click left mouse button
	- draw link: hold space + move mouse to room
	- assign start room: hold S + click on room
	- assign end room: hold E + click on room
	- undo action: backspace
	- erase everything: delete
	- run algorithm: enter
	
